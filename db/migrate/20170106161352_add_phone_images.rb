class AddPhoneImages < ActiveRecord::Migration[5.0]
  def change
  	create_table :phone_images do |t|
    	t.attachment :image
    	t.integer :priority,default: 1
    	t.timestamps
    end
    add_reference :phone_images,:phone,index: true
	add_foreign_key :phone_images, :phones, on_delete: :cascade
  end
end
