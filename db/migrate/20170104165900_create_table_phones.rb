class CreateTablePhones < ActiveRecord::Migration[5.0]
  def change
    create_table :phones do |t|
    	t.string :name
    	t.string :alias_name
    	t.integer :priority,default: 1
      t.datetime :release_date
      t.attachment :avatar
    	t.timestamps
	end
	add_reference :phones,:brand,index: true
	add_foreign_key :phones, :brands, on_delete: :cascade
  end
end
