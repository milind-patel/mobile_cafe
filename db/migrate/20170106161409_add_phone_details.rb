class AddPhoneDetails < ActiveRecord::Migration[5.0]
  def change
  	create_table :phone_details do |t|
    	t.string :technology,default: "GSM / HSPA / LTE"
    	t.boolean :gprs,default: true
    	t.boolean :edge ,default: true
    	t.datetime :announcement_date
    	t.string :status
    	t.datetime :status_date,default: Time.now.to_date
    	t.string :dimensions,default: "142.8 x 69.5 x 8.1 mm (5.62 x 2.74 x 0.32 in)"
    	t.string :weight,default: "143 g (5.04 oz)"
    	t.string :sim_type ,default: "Nano-SIM"
    	t.string :sim,default: "Dual SIM (Nano-SIM, dual stand-by)"
    	t.string :screen_type,default: "IPS LCD capacitive touchscreen, 16M colors"
    	t.string :size,default: "5.0 inches (~69.4% screen-to-body ratio)"
    	t.string :resolution,default: "720 x 1280 pixels (~294 ppi pixel density)"
    	t.string :memory_card_supported,default: "up to 256 GB (dedicated slot)"
    	t.string :memory_card_type ,default: "microSD"
    	t.string :alert_types,default: "Vibration; MP3, WAV ringtones"
    	t.boolean :loudspeaker ,default: true
    	t.string :wlan,default: "Wi-Fi 802.11 b/g/n, Wi-Fi Direct, hotspot"
    	t.string :bluetooth,default: "v4.1, A2DP"
    	t.boolean :gps_flag,default: true
    	t.string :gps,default: "with A-GPS, GLONASS/ BDS (region dependent)"
    	t.string :radio,default: "Stereo FM radio"
    	t.string :usb ,default: "microUSB v2.0, USB On-The-Go"
    	t.string :messaging,default: "SMS(threaded view), MMS, Email, Push Mail, IM"
    	t.string :browser,default: "HTML5"
    	t.boolean :java,default: false
    	t.string :battery,default: "Non-removable Li-Ion 2400 mAh battery"
    	t.string :colors,default: "Black, White"
    	t.string :sensors,default: "Fingerprint, accelerometer, proximity"
    	t.string :cpu,default: "Quad-core 1.4 GHz Cortex-A53"
    	t.string :internal_memory,default: "16 GB"
    	t.string :ram,default: "2 GB"
    	t.string :os,default: "Android OS, v6.0.1 (Marshmallow)"
    	t.string :primary_camera,default: "13 MP"
    	t.string :primary,default: "f/1.9, 28mm, autofocus, LED flash"
    	t.string :video,default: "1080p@30fps"
    	t.string :secondary_camera,default: "5 MP, f/2.2"
    	t.string :speed,default: "HSPA, LTE Cat4 150/50 Mbps"
    	t.string :features,default: "Geo-tagging, touch focus, face detection, panorama, HDR"
    	t.string :protection,default: "Corning Gorilla Glass"
    	t.boolean :multitouch,default: true
    	t.string :_2g_bands,default: "GSM 850 / 900 / 1800 / 1900 - SIM 1 & SIM 2"
    	t.string :_3_5mm_jack_,default: "Yes"
    	t.string :_3g_bands,default: "HSDPA 850 / 900 / 1900 / 2100 "
    	t.string :_4g_bands,default: "LTE band 1(2100), 3(1800), 5(850), 7(2600), 8(900), 20(800), 38(2600), 40(2300)"
        t.string :chipset
        t.string :price_in_rupees
        t.string :price_in_dollar
    end
    add_reference :phone_details,:phone,index: true
	add_foreign_key :phone_details, :phones, on_delete: :cascade
  end
  	
end
