Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"
  resources :brands,except: [:show,:new,:create,:update,:destroy] do
    collection do
      get "brand_details"
      get "/:brand/:phone" => "brands#phone_details"
    end
    member do
      get "phone_details"
    end
  end
  resources :brands_data
  #get ':alias_name/:phone_alias_name' => "phones#show"
  #get ':alias_name' => "brands#show"
  get '/brands/:id' => "brands#brand_details"
  #get '/brands/:brand_name/:id' => "phones#phone_details"
  resources :phone_details
  resources :phones #,param: :alias_name
  resources :articles,path: 'blog'
end
