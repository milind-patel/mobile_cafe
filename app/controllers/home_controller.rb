class HomeController < ApplicationController
	def index
		@brands = Brand.all.order("priority ASC")
	end
end
