class PhonesController < ApplicationController
  before_action :set_phone, only: [:show, :edit, :update, :destroy]

  # GET /phones
  # GET /phones.json
  def index
    @phones = Phone.all.order("brand_id ASC")
  end

  # GET /phones/1
  # GET /phones/1.json
  def show
    # if params[:alias_name] == "blog"
    #   @post = Article.new
    #   render :template => "articles/new"
    # else
    #   @phone_detail = @phone.phone_details.first
    # end
    @phone_detail = @phone.phone_detail
  end

  # GET /phones/new
  def new
    @phone = Phone.new
  end

  # GET /phones/1/edit
  def edit
  end

  # POST /phones
  # POST /phones.json
  def create
    @phone = Phone.new(phone_params)
    @phone.alias_name = @phone.name
    respond_to do |format|
      if @phone.save
        format.html { redirect_to phone_path(alias_name: @phone.alias_name.gsub(" ","_")), notice: 'Phone was successfully created.' }
        format.json { render :show, status: :created, location: @phone }
      else
        format.html { render :new }
        format.json { render json: @phone.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /phones/1
  # PATCH/PUT /phones/1.json
  def update
    params[:phone][:alias_name] = params[:phone][:name]
    respond_to do |format|
      if @phone.update(phone_params)
        format.html { redirect_to phones_path, notice: 'Phone was successfully updated.' }
        format.json { render :show, status: :ok, location: @phone }
      else
        format.html { render :edit }
        format.json { render json: @phone.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /phones/1
  # DELETE /phones/1.json
  def destroy
    @phone.destroy
    respond_to do |format|
      format.html { redirect_to phones_url, notice: 'Phone was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_phone
      if params[:phone_alias_name].present?
        @phone = Phone.find_by(alias_name:  params[:phone_alias_name].gsub("_"," "))
      else
        #@phone = Phone.find_by(alias_name:  params[:alias_name].gsub("_"," "))
        @phone = Phone.find(params[:id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def phone_params
      params.fetch(:phone).permit(:name,:image,:alias_name,:brand_id,:release_date)
    end
end
