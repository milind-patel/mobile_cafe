class PhoneDetailsController < ApplicationController
  before_action :set_phone_detail, only: [:show, :edit, :update, :destroy]

  # GET /phone_details
  # GET /phone_details.json
  def index
    @phone_details = PhoneDetail.all
  end

  # GET /phone_details/1
  # GET /phone_details/1.json
  def show
  end

  # GET /phone_details/new
  def new
    @phone_detail = PhoneDetail.new
  end

  # GET /phone_details/1/edit
  def edit
  end

  # POST /phone_details
  # POST /phone_details.json
  def create
    @phone_detail = PhoneDetail.new(phone_detail_params)

    respond_to do |format|
      if @phone_detail.save
        format.html { redirect_to @phone_detail, notice: 'Phone detail was successfully created.' }
        format.json { render :show, status: :created, location: @phone_detail }
      else
        format.html { render :new }
        format.json { render json: @phone_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /phone_details/1
  # PATCH/PUT /phone_details/1.json
  def update
    respond_to do |format|
      if @phone_detail.update(phone_detail_params)
        format.html { redirect_to @phone_detail, notice: 'Phone detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @phone_detail }
      else
        format.html { render :edit }
        format.json { render json: @phone_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /phone_details/1
  # DELETE /phone_details/1.json
  def destroy
    @phone_detail.destroy
    respond_to do |format|
      format.html { redirect_to phone_details_url, notice: 'Phone detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_phone_detail
      @phone_detail = PhoneDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def phone_detail_params
      params.fetch(:phone_detail).permit(:phone_id,:display_size,:display_resolution,:display_type,:display_multitouch,
        :ram,:chipset,:battery_capacity,:battery_technology,:technology,"2g_bands","3g_bands","4g_bands",:gprs,:edge,
        :announced,:status,:dimensions,:weight,:sim,:os,:cpu,:gpu,:card_slot,:internal_memory,:primary_camera,:camera_features,
        :video,:secondary_camera,:alert_types,:loud_speaker,"3.5mm_jack",:wlan,:bluetooth,:gps,:nfc,:sensors,:messaging,:browser,:java,
        :colors,:price_group,:image)
    end
end
