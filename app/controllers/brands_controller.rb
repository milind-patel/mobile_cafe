class BrandsController < ApplicationController
  before_action :set_brand, only: [:edit, :update, :destroy]

  def brand_details
    @brand = Brand.find_by(alias_name: params[:id]) #if params[:alias_name].present?
    #@brand = Brand.find(params[:id]) if params[:alias_name].blank?
    @phones = @brand.phones #.order("release_date DESC")
    @latest_releases = @phones #.order #("release_date ASC").limit(4)
  end

  def phone_details
    @phone = Phone.find_by(alias_name: params[:phone])
    @phone_detail = @phone.phone_detail

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_brand
      @brand = Brand.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def brand_params
      params.fetch(:brand).permit(:image,:logo,:name,:description,:alias_name,:priority)
    end
end
