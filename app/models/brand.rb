class Brand < ApplicationRecord
	has_many :phones,dependent: :destroy
	#has_many :phones
	has_many :phone_details
	#has_attached_file :image, styles: { large: "500x500>", medium: "420x420>", thumb: "100x100>" }
	#validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	has_attached_file :logo, styles: { thumb: "100x100>" }
	validates_attachment_content_type :logo, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

	
	def Brand.update_priority(brand_obj)
		#if brand_obj.priority_changed?
			all_brands = Brand.all
			lesser_priority = all_brands.collect{|t| t if t.priority < brand_obj.priority}.reject{|t| t.nil?}
			same_priority = all_brands.collect{|t| t if t.priority == brand_obj.priority}.reject{|t| t.nil?}
			bigger_priority = all_brands.collect{|t| t if t.priority > brand_obj.priority}.reject{|t| t.nil?}
			same_priority.each_with_index do |obj,index|
				if obj.id != brand_obj.id
					obj.update_attributes(priority: lesser_priority.size + 2)
				end
			end
			default_val = lesser_priority.size + 2 
			bigger_priority.each_with_index do |obj,index|
				obj.update_attributes(priority:  default_val + 1)
				default_val = default_val + 1
			end


			# all_brands.each_with_index do |brand,index|
			# 	if brand.priority < brand_obj.priority
			# 	elsif brand.priority == brand_obj.priority && brand.id != brand_obj.id
			# 		brand.update_attributes(priority: brand.priority + 1)
			# 	elsif brand.priority > brand_obj.priority
			# 		brand.update_attributes(priority: brand.priority + 1)
			# 	end
			# end
		#end
	end
end
