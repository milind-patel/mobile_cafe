class Phone < ApplicationRecord
	belongs_to :brand
	has_one :phone_detail
	has_many :phone_images
	has_attached_file :avatar, styles: { thumb: "100x100>" }
	validates_attachment_content_type :avatar, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
